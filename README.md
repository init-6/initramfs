initramfs
=========

If you've never worked with a initramfs, please read [Gentoo Linux Initial RAM File System Guide](http://www.gentoo.org/doc/en/initramfs-guide.xml), [Initramfs on Gentoo-wiki.com](http://en.gentoo-wiki.com/wiki/Initramfs), [Early userspace mounting](http://wiki.gentoo.org/wiki/Early_Userspace_Mounting)

The main configuration file is symlink [scripts/config](https://github.com/init6/initramfs/blob/master/scripts/config)

A typical use:

    make prep
    cd initramfs-*

Make the necessary changes… 

    cd ..
    make build
    cp /usr/src/linux/initrd-0.0.1.cpio.xz /boot/initrd-0.0.1.cpio.xz
