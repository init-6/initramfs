#!/bin/bash

#
# An library for init script
#

#
#  Copyright © 2012, 2013 Andrey Ovcharov <http://sudormrf.wordpress.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  The latest version of this software can be obtained here:
#
#  https://github.com/init6/initramfs
#

# Load elog functions.
[ -f /lib64/core-functions.sh ] && source /lib64/core-functions.sh

BR="\x1b[0;01m"
#BLUEDARK="\x1b[34;0m"
BLUE="\x1b[34;01m"
#CYANDARK="\x1b[36;0m"
CYAN="\x1b[36;01m"
#GRAYDARK="\x1b[30;0m"
#GRAY="\x1b[30;01m"
#GREENDARK="\x1b[32;0m"
#GREEN="\x1b[32;01m"
#LIGHT="\x1b[37;01m"
#MAGENTADARK="\x1b[35;0m"
#MAGENTA="\x1b[35;01m"
NORMAL="\x1b[0;0m"
#REDDARK="\x1b[31;0m"
RED="\x1b[31;01m"
YELLOW="\x1b[33;01m"

run_shell() {
	/bin/ash
}

rescue_shell() {
	if [ $rescueshell = 'false' ]; then
		ewarn "$@"
		ewarn "Dropping to rescueshell because of above error."
	fi
	ewarn "Rescue Shell (busybox's ${BLUE}/bin/sh${NORMAL})"
	ewarn "To reboot, press 'control-alt-delete'."
	ewarn "If you wish continue booting process, just exit from this shell."
	busybox --install -s
	run_shell
}

uppercase(){
	# needs tr on busybox
	echo $1 | tr 'a-z' 'A-Z'
}

mount_sysfs() {
	local ret

	mount -t sysfs sysfs /sys -o noexec,nosuid,nodev >/dev/null 2>&1
	ret=$?
	[ ${ret} -eq 0 ] || eerror "Failed to mount ${BLUE}/sys${NORMAL}!"
}

doice(){
	if [ ! -f /sys/power/tuxonice/do_resume ]; then
		ewarn "Your kernel do not support TuxOnIce";
	else
		einfo "Sending do_resume signal to TuxOnIce"
		echo 1 > /sys/power/tuxonice/do_resume
	fi
}

doluks(){
	einfo "Opening encrypted partition and mapping to ${BLUE}/dev/mapper/enc_root${NORMAL}"
	if [ -f /root/keyfile ]; then
		/sbin/cryptsetup --key-file=/root/keyfile luksOpen "${crypt_root}" enc_root || rescue_shell
	else
		/sbin/cryptsetup luksOpen "${crypt_root}" enc_root || rescue_shell
	fi
	
}

dodir() {
	for dir in $*; do
		mkdir -p $dir
	done
}

dolvm() {
#	mkdir -p /etc/lvm
#	echo 'devices { filter = [ "a|/dev/md[0-9]+$|", "a|/dev/[hsv]d[a-z][0-9]*$|", "r/.*/" ] }' > /etc/lvm/lvm.conf
	einfo "Scaning all disks for volume groups"
	/sbin/lvm vgscan --mknodes
	einfo "Finding lvm devices..."
	/sbin/lvm vgchange -ay --sysinit
	/sbin/lvm vgmknodes --ignorelockingfailure
}

doraid() {
	einfo "Scaning for software raid arrays"
	mdadm --assemble --scan
	mdadm --auto-detect
}

uuidlabel_root() {
	for cmd in $(cat /proc/cmdline) ; do
		case $cmd in
		root=*)
			type=$(echo $cmd | cut -d= -f2)
			if [ $type == "LABEL" ] || [ $type == "UUID" ] ; then
				uuid=$(echo $cmd | cut -d= -f3)
				mount -o ro $(blkid |grep "$type=\"$uuid\"" | cut -d: -f1 | grep -v '/dev/dm-[0-9]*$\|-real$' | sort | head -n1 ) /mnt/root || rescue_shell
			else
				mount -o ro $(echo $cmd | cut -d= -f2) /mnt/root || rescue_shell
			fi
		;;
		esac
	done
}

check_filesystem() {
	# most of code coming from /etc/init.d/fsck

	local fsck_opts= check_extra= RC_UNAME=$(uname -s)

	# FIXME : get_bootparam forcefsck
	if [ -e /forcefsck ]; then
		fsck_opts="$fsck_opts -f"
		check_extra="(check forced)"
	fi

	echo "Checking local filesystem $check_extra : $1"

	if [ "$RC_UNAME" = Linux ]; then
		fsck_opts="$fsck_opts -C0 -T"
	fi

	trap : INT QUIT
	/sbin/fsck -p $fsck_opts $1
	case $? in
	0)	return 0;;
	1)	einfo "Filesystems repaired"; return 0;;
	2|3)	if [ "$RC_UNAME" = Linux ]; then
		 	ewarn "Filesystems repaired, but reboot needed"
			 	reboot -f
		else
			rescue_shell "Filesystems still have errors;" \
				"manual fsck required"
		fi;;
	4)	if [ "$RC_UNAME" = Linux ]; then
			rescue_shell "Fileystem errors left uncorrected, aborting"
		else
		 	ewarn "Filesystems repaired, but reboot needed"
			reboot
		fi;;
	8)	einfo "Operational error"; return 0;;
	12)	einfo "fsck interrupted";;
	*)	eerror "Filesystems couldn't be fixed";;
	esac
	rescue_shell || return 1
}

