#!/bin/bash

#
# Compatibility functions from sys-apps/openrc
#

#
#  Copyright © 2012, 2013 Andrey Ovcharov <http://sudormrf.wordpress.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  The latest version of this software can be obtained here:
#
#  https://raw.github.com/init6/core-functions/master/core-functions.sh
#

# Shamelessly stolen from /etc/init.d/functions.sh
for arg; do
	case "$arg" in
		--nocolor|--nocolour|-C)
		export EINFO_COLOR="NO"
		;;
	esac
done

# Adapted from /etc/init.d/functions.sh so we don't need eval_colors
if [ "${EINFO_COLOR}" != NO ]; then
	if [ -z "$GOOD" ]; then
		GOOD=$(echo -ne "\e[1;32m")
		WARN=$(echo -ne "\e[1;33m")
		BAD=$(echo -ne "\e[1;31m")
		HILITE=$(echo -ne "\e[1;36m")
		BRACKET=$(echo -ne "\e[1;34m")
		NORMAL=$(echo -ne "\e[0;0m")
		export GOOD WARN BAD HILITE BRACKET NORMAL
	fi
fi

# Hack to get terminal cursor position. I believe it's OK to use it
# since OpenRC uses terminal escape sequences anyhow
function curpos() {
	echo -ne "\e[6n"
	read -sdR CURPOS
	CURPOS=${CURPOS#*[}

	c=0
	for e in $(echo $CURPOS | tr ";" "\n"); do
	arr[c]="${e}"
	c=$((c + 1))
	done

	case "$1" in
	"row")
		return "${arr[0]}"
		;;
	"col")
		return "${arr[1]}"
		;;
	*)
		return "${CURPOS}"
		;;
	esac

	return "${CURPOS}"
}

#
# this function was lifted from openrc. It returns 0 if the argument  or
# the value of the argument is "yes", "true", "on", or "1" or 1
# otherwise.
#
function yesno() {
	[ -z "$1" ] && return 1

	case "$1" in
		[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee]|[Oo][Nn]|1) return 0;;
		[Nn][Oo]|[Ff][Aa][Ll][Ss][Ee]|[Oo][Ff][Ff]|0) return 1;;
	esac

	local value=
	eval value=\$${1}
	case "$value" in
		[Yy][Ee][Ss]|[Tt][Rr][Uu][Ee]|[Oo][Nn]|1) return 0;;
		[Nn][Oo]|[Ff][Aa][Ll][Ss][Ee]|[Oo][Ff][Ff]|0) return 1;;
		*) vewarn "\$$1 is not set properly"; return 1;;
	esac
}

function elog() {
	if [ "${EINFO_QUIET}" == "true" ]; then
		return
	fi
	echo -ne "${@}"
}

function errlog() {
	echo -ne "${@}" > /dev/stderr
}

#
#	use the system logger to log a message
#
function esyslog()
{
	local pri=
	local tag=

	if [[ -x /usr/bin/logger ]] ; then
		pri="$1"
		tag="$2"

		shift 2
		[[ -z "$*" ]] && return 0

		/usr/bin/logger -p "${pri}" -t "${tag}" -- "$*"
	fi

	return 0
}

#
#	show an informative message (without a newline)
#
function einfon() {
	elog " ${GOOD}*${NORMAL} ${_ELOG_INDENT}${@}"
}

#
#	show an informative message (with a newline)
#
function einfo() {
	elog " ${GOOD}*${NORMAL} ${_ELOG_INDENT}${@}\n"
}

#
#	show a warning message (with a newline)
#
function ewarn() {
	errlog " ${WARN}*${NORMAL} ${_ELOG_INDENT}${@}\n"
}

function eerror() {
	errlog " ${BAD}*${NORMAL} ${_ELOG_INDENT}${@}\n"
}

# v-e-commands honor EINFO_VERBOSE which defaults to no.
# The condition is negated so the return value will be zero.
function veinfo() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || einfo "$@";
}

function veinfon() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || einfon "$@";
}

function vewarn() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || ewarn "$@";
}

function veerror() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || eerror "$@";
}

function vebegin() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || ebegin "$@";
}

function veend() {
	[[ ${EINFO_VERBOSE} == "yes" ]] && { eend "$@"; return $?; }
	return ${1:-0}
}

function vewend() {
	[[ ${EINFO_VERBOSE} == "yes" ]] && { ewend "$@"; return $?; }
	return ${1:-0}
}

function veindent() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || eindent;
}

function veoutdent() {
	[[ ${EINFO_VERBOSE} != "yes" ]] || eoutdent;
}

#
#	show a message indicating the start of a process
#
function ebegin() {
	elog " ${GOOD}*${NORMAL} ${_ELOG_INDENT}${@} ...\n"
}

#
#	indicate the completion of process
#	if error, show errstr via eerror
#
function eend() {
	if [ "${EINFO_QUIET}" == "true" ]; then
		return
	fi
	msg="$1"
	if [ ! -z "${msg##*[!0-9]*}" ]; then
		retval="$msg"
	else
		eerror "$msg"
		retval=1
	fi
	# go up
	echo -ne "\e[1A"
	[ -f /bin/busybox ] && COLS=$(/bin/busybox ttysize 2>/dev/null | cut -d' ' -f1) || COLS=${COLUMNS:-128} # bash's internal COLUMNS variable
#	[ -f /bin/stty ] && COLS=$(/bin/stty size 2>/dev/null | cut -d' ' -f2) # coreutils dependency
#	[ -f /bin/tput ] && COLS=$(/bin/tput cols) # ncurses dependency
	ENDCOL=$((COLS - 6))
	# goto column
	echo -ne "\e[${ENDCOL}C"
	LBRAC="${BRACKET}[${NORMAL}"
	RBRAC="${BRACKET}]${NORMAL}"
	if [ "$retval" != 1 ]; then
		echo -e "${LBRAC} ${GOOD}ok${NORMAL} ${RBRAC}"
	else
		echo -e "${LBRAC} ${BAD}!!${NORMAL} ${RBRAC}"
	fi
}

#
#	increase the indent used for e-commands.
#
function eindent() {
	if [ -z "${_ELOG_INDENT}" ]; then
		export _ELOG_INDENT="  "
	else
		export _ELOG_INDENT="${_ELOG_INDENT}  "
	fi
}

#
#	decrease the indent used for e-commands.
#
function eoutdent() {
	if [ -z "${_ELOG_INDENT}" ]; then
		unset _ELOG_INDENT
	else
		export _ELOG_INDENT=$(echo "${_ELOG_INDENT}" | sed "s/  //")
	fi
}

#
#	prints the current libdir {lib,lib32,lib64}
#
function get_libdir() {
	if [[ -n ${CONF_LIBDIR_OVERRIDE} ]] ; then
		CONF_LIBDIR="${CONF_LIBDIR_OVERRIDE}"
	elif [[ -x /usr/bin/portageq ]] ; then
		CONF_LIBDIR="$(/usr/bin/portageq envvar CONF_LIBDIR)"
	fi
	echo "${CONF_LIBDIR:=lib}"
}

#
#   return 0 if gentoo=param was passed to the kernel
#
#   EXAMPLE:  if get_bootparam "nodevfs" ; then ....
#
function get_bootparam() {
	local x copt params retval=1

	[[ ! -r /proc/cmdline ]] && return 1

	for copt in $(< /proc/cmdline) ; do
		if [[ ${copt%=*} == "gentoo" ]] ; then
			params=$(gawk -v PARAMS="${copt##*=}" '
				BEGIN {
					split(PARAMS, nodes, ",")
					for (x in nodes)
						print nodes[x]
				}')

			# Parse gentoo option
			for x in ${params} ; do
				if [[ ${x} == "$1" ]] ; then
#					echo "YES"
					retval=0
				fi
			done
		fi
	done

	return ${retval}
}

#
#   return 0 if any of the files/dirs are newer than
#   the reference file
#
#   EXAMPLE: if is_older_than a.out *.o ; then ...
function is_older_than() {
	local x=
	local ref="$1"
	shift

	for x in "$@" ; do
		[[ ${x} -nt ${ref} ]] && return 0
		[[ -d ${x} ]] && is_older_than "${ref}" "${x}"/* && return 0
	done

	return 1
}

export -f curpos ebegin eend eerror eindent einfo einfon elog eoutdent errlog esyslog ewarn get_bootparam get_libdir is_older_than vebegin veend veerror veindent veinfo veinfon veoutdent vewarn vewend yesno
