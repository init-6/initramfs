PACKAGE=initramfs
VERSION=0.0.1
#RELEASE=1
CWD=$(CURDIR)
DESTARCH=$(CWD)/initramfs-$(VERSION)
#RELEASE=$(shell date +%s)

clean:
	@if [ -d $(DESTARCH) ]; then echo "About to remove $(DESTARCH) in 5 seconds ..."; sleep 5; fi
	@rm -rf $(DESTARCH)

prep:
	@mkdir -p $(DESTARCH)
	@cp -r src_root/* $(DESTARCH)
	@find $(DESTARCH) -type f -name '.gitignore' -exec rm {} \;

	@(cd scripts && \
	sh cp_all_need_stuff)

	@(cd /usr/src/linux && \
	sh scripts/gen_initramfs_list.sh $(DESTARCH) | egrep -v ".git|.gitignore|filelist.txt" > $(DESTARCH)/filelist.txt )

build:
	@(cd /usr/src/linux && \
	sh scripts/gen_initramfs_list.sh -o initrd-$(VERSION).cpio.xz $(DESTARCH)/filelist.txt )
